export default class SwapiService {

    _apiBase = 'https://swapi.dev/api';
    _imageBase = 'https://starwars-visualguide.com/assets/img';
  
    getResource = async (url) => {
      const response = await fetch(`${this._apiBase}${url}`);
      if(!response.ok){
        throw new Error(`Could not fetch ${url}
        Received: ${response.status}`)
      }
      const body = await response.json();
      return body;
    }
  
    getAllPeople = async () => {
      const body = await this.getResource(`/people`);
      return body.results.map(this._transformPerson);
    }
  
    getPerson = async (id) => {
      const apiPerson = await this.getResource(`/people/${id}`);
      return this._transformPerson(apiPerson);
    }
  
    getAllPlanets = async () => {
      const body = await this.getResource(`/planets`);
      return body.results.map(this._transformPlanet);
    }
  
    getPlanet = async (id) => {
      const apiPlanet = await this.getResource(`/planets/${id}`);
      return this._transformPlanet(apiPlanet);
    }
  
    getAllStarships = async () => {
      const body = await this.getResource(`/starships`);
      return body.results.map(this._transformStarship);
    }
  
    getStarship = async (id) => {
      const apiShip = await this.getResource(`/starships/${id}`);
      return this._transformStarship(apiShip);
    }

    /////////////

    _extractId(item){
      const idRegExp = /\/([0-9]*)\/$/;
      return item.url.match(idRegExp)[1];
    }

    _transformPlanet = (planet) => {     
      return {
          id: this._extractId(planet),
          name: planet.name,
          population: planet.population,
          rotationPeriod: planet.rotation_period,
          diameter: planet.diameter,
      }
    }

    _transformPerson = (person) => {
      //console.log(person);    
      return {
          id: this._extractId(person),
          name: person.name,
          gender: person.gender,
          birthYear: person.birth_year,
          eyeColor: person.eye_color,
      }
    }

    _transformStarship = (starship) => {    
      return {
          id: this._extractId(starship),
          name: starship.name,
          model: starship.model,
          manufacturer: starship.manufacturer,
          costInCredits: starship.cost_in_credits,
          length: starship.length,
          crew: starship.crew,
          passengers: starship.passengers,
          cargoCapacity: starship.cargo_capacity,
      }
    }

    getImageUrl = (item) => {
      
    }

    getPersonImage = ({id}) => {
      return `${this._imageBase}/characters/${id}.jpg`;
    }

    getStarshipImage = ({id}) => {
      return `${this._imageBase}/starships/${id}.jpg`;
    }

    getPlanetImage = ({id}) => {
      return `${this._imageBase}/planets/${id}.jpg`;
    }
    
  }