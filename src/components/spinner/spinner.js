import React, { Component } from 'react';
import './spinner.css';

export default class Spinner extends Component {
    render() {
        return (
            <div className="spinner">
               <div className="loadingio-spinner-dual-ring-aiwvifvd0tb"><div className="ldio-91d8ciko2rs">
                    <div></div><div><div></div></div>
                    </div>
                </div>
            </div>
        )
    }
}
