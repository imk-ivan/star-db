import React, { Component } from 'react';
import Row from '../../row';
import { PlanetList, PlanetDetails } from '../../sw-components';

export default class PlanetsPage extends Component {

    state = {
        selectedItemId: 1,
    }

    onItemSelected = (e,id) => {
        e.preventDefault();
        this.setState({
            selectedItemId: id
        });
    }   

    render() {

        const itemList = (
            <PlanetList onItemSelected={this.onItemSelected} />
        )

        const personDetails = (
            <PlanetDetails itemId={this.state.selectedItemId}/>
        )

        return (
            <Row leftElement={itemList} rightElement={personDetails}/> 
        )
    }
}
