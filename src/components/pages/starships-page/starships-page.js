import React, { Component } from 'react';
import Row from '../../row';
import { StarshipDetails, StarShipList } from '../../sw-components';

export default class StarshipsPage extends Component {

    state = {
        selectedItemId: 2,
    }

    onItemSelected = (e,id) => {
        e.preventDefault();
        this.setState({
            selectedItemId: id
        });
    }   

    render() {

        const itemList = (
            <StarShipList onItemSelected={this.onItemSelected} />
        )

        const personDetails = (
            <StarshipDetails itemId={this.state.selectedItemId}/>
        )

        return (
            <Row leftElement={itemList} rightElement={personDetails}/> 
        )
    }
}
