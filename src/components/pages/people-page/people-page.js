import React, { Component } from 'react';
import Row from '../../row';
import { PersonDetails, PersonList } from '../../sw-components';

export default class PeoplePage extends Component {

    state = {
        selectedItemId: 1,
    }

    onItemSelected = (e,id) => {
        e.preventDefault();
        this.setState({
            selectedItemId: id
        });
    }   

    render() {

        const itemList = (
            <PersonList onItemSelected={this.onItemSelected} />
        )

        const personDetails = (
            <PersonDetails itemId={this.state.selectedItemId}/>
        )

        return (
            <Row leftElement={itemList} rightElement={personDetails}/> 
        )
    }
}
