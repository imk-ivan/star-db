import React from 'react';
import ItemList from '../item-list';
import { withData } from '../hoc-helpers';
import { withSwapiService } from '../hoc-helpers';

const withChildFunction = (Wrapped, fn) => {
    return (props) => {
        return(
            <Wrapped {...props}>
                {fn}
            </Wrapped>
        )
    }
}

const renderName = ({name}) => `${name}`;
const renderPersonName = (item) => `${item.name} (${item.gender})`;

const PersonList = 
        withSwapiService(
            withData(withChildFunction(ItemList,renderPersonName)),
            (swapiService) => {
                return{
                    getData: swapiService.getAllPeople
                }
            }
        );

const PlanetList = 
        withSwapiService(
            withData(withChildFunction(ItemList,renderName)),
            (swapiService) => {
                return{
                    getData: swapiService.getAllPlanets
                }
            }
        );

const StarShipList =
        withSwapiService(
            withData(withChildFunction(ItemList,renderName)),
                (swapiService) => {
                    return{
                        getData: swapiService.getAllStarships
                    }
            }
        );

export {
    PersonList,
    PlanetList,
    StarShipList,
    withChildFunction,
}