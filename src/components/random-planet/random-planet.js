import React, { Component } from 'react'
import './random-planet.css';
import SwapiService from '../../services/swapi-service';
import Spinner from '../spinner';
import ErrorIndicator from '../error-indicator';

export default class RandomPlanet extends Component {

    swapiService = new SwapiService();

    state = {
        loading: true,
        planet: {},
    };

    componentDidMount(){
        this.updatePlanet();
        this.interval = setInterval(this.updatePlanet, 5000);
    }

    componentWillUnmount(){
        clearInterval(this.interval);
    }

    onPlanetLoaded = (planet) => {
        this.setState({
            planet,
            loading: false,
            error: false,
        });
    };

    onError = (err) => {
        this.setState({
            error:true,
            loading: false,
        });
    };
    

    updatePlanet = () => {
        const id = Math.floor(Math.random()*25) + 3;
        this.swapiService
            .getPlanet(id)
            .then(this.onPlanetLoaded)
            .catch(this.onError);
    };
    

    render() {

        const { planet, loading, error} = this.state;
        const hasData = !loading && !error;
        return (
            <div className="random-planet jumbotron">
                { hasData ? <PlanetView planet={planet}/> : null}
                { loading ? <Spinner/> : null }
                { error ? <ErrorIndicator/> : null }
            </div>
        )
    }
}

const PlanetView = ({planet}) => {
    const { id, name, population, rotationPeriod, diameter } = planet;
    return (
        <React.Fragment>
            <div className="planet-img">
                <img src={`https://starwars-visualguide.com/assets/img/planets/${id}.jpg`} alt={name} />
            </div>
            <div className="planet-info">
                <div className="planet-name">{name}</div>
                <ul className="info-list list-group list-group-flush">
                    <li className="list-group-item">
                        <span>Population: </span>
                        <span>{population}</span>
                    </li>
                    <li className="list-group-item">
                        <span>Rotation period: </span>
                        <span>{rotationPeriod}</span>
                    </li>
                    <li className="list-group-item">
                        <span>Diameter: </span>
                        <span>{diameter}</span>
                    </li>
                </ul>
            </div>
        </React.Fragment>
    )
}

