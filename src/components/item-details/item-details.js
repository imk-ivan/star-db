import React, { Component } from 'react';
import SwapiService from '../../services/swapi-service';
import Spinner from '../spinner';
import './item-details.css';

const Record = ({item, field, label}) => {
    return(
        <li className="list-group-item">
            <span>{label}: </span>
            <span>{item[field]}</span>
        </li>
    )
}

export {
    Record
}

export default class ItemDetails extends Component {

    swapiService = new SwapiService();

    state = {
        item: null,
        loading: false,
        image: null,
    };

    componentDidMount(){
        this.updateItem();
    }

    componentDidUpdate(prevProps){
        if(this.props.itemId !== prevProps.itemId){
            this.updateItem();
        }    
    }

    updateItem = () => {
        this.setState({loading: true})

        const { itemId, getData, getImageUrl } = this.props;

        if(itemId){
            getData(itemId)
            .then((item) => {
                this.setState({item, image: getImageUrl(item), loading: false});
            })
            .catch(()=>{this.setState({loading: false})});
        }
    }

    render() {

        if(!this.state.item){
            return <span>Please select a person from the list.</span>;
        }
        return (
            <div className="item-details">
                {!this.state.loading 
                    ? <ItemView item={this.state.item} image={this.state.image}>
                            {this.props.children}
                       </ItemView> 
                    :<Spinner/>
                }
            </div>
        )
    }
}

const ItemView = (props) => {
    const {image, item} = props;
    return(
        <React.Fragment>
            <div className="item-img">
                    <img src={image} alt={item.name}/>
            </div>
            <div className="item-info">
                <div className="item-name">{item.name}</div>
                <ul className="info-list list-group list-group-flush">
                    {
                        React.Children.map(props.children, (child) => {
                            return React.cloneElement(child,{item});
                        })
                    }
                </ul>
            </div>
        </React.Fragment>
    )
}
