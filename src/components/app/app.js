import React, { Component } from 'react';
import Header from '../header';
import RandomPlanet from '../random-planet';
import './app.css';
import PeoplePage from '../pages/people-page/people-page';
import SwapiService from '../../services/swapi-service';
import { SwapiServiceProvider } from '../swapi-service-context';
import PlanetsPage from '../pages/planets-page/planets-page';
import StarshipsPage from '../pages/starships-page/starships-page';
import ErrorBoundry from '../error-boundry';

export default class App extends Component{

    state = {
        selectedPersonId: null,
        swapiService: new SwapiService(),
    }

    onPersonSelected = (e,id) => {
        e.preventDefault();
        this.setState({
            selectedPersonId: id
        });
        
        console.log('person - ', id);
    }

    render(){

        return (
            <div className="app">
                <div className="container">
                    <ErrorBoundry>
                        <SwapiServiceProvider value={this.state.swapiService}>
                            <Header/>
                            <RandomPlanet/>
                            <PeoplePage/>
                            <PlanetsPage/>
                            <StarshipsPage/>
                        </SwapiServiceProvider>
                    </ErrorBoundry>
                </div>
            </div>
        )
    }
};
