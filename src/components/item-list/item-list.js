import React from 'react'
import './item-list.css';

const ItemList = (props) => {

    const { data, onItemSelected, children } = props;

    const items = data.map((item) => {
        const {id} = item;
        const label = children(item);
        return(
            <li className="list-group-item"
                key={id}>
                <span
                    onClick={(e) => onItemSelected(e,id)}>
                    {label}
                </span>
            </li>
        )
    })

    return (
        <div className="item-list">
            <ul className="list-group">
                {items}
            </ul>
        </div>
    )
}

export default ItemList;