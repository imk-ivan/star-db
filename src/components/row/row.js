import React from 'react'

function Row({leftElement, rightElement}) {
    return (
        <div className="row">
            <div className="col-sm-6">
                {leftElement}
            </div>
            <div className="col-sm-6">
                {rightElement}
            </div>
        </div>
    )
}

export default Row;
